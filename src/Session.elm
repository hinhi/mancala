module Session exposing (Session, fromNavKey, navKey)

import Browser.Navigation as Nav


type Session
    = Session Nav.Key


fromNavKey : Nav.Key -> Session
fromNavKey key =
    Session key


navKey : Session -> Nav.Key
navKey session =
    case session of
        Session key ->
            key
