module Page.Game exposing (..)

import Html exposing (Html)
import Session exposing (Session)



-- MODEL


type alias Model =
    { session : Session }


init : Session -> ( Model, Cmd Msg )
init session =
    ( Model session, Cmd.none )



-- VIEW


view : Model -> { title : String, content : Html msg }
view _ =
    { title = "game"
    , content = Html.text "Game"
    }



-- UPDATE


type Msg
    = A


update : Msg -> Model -> ( Model, Cmd Msg )
update _ model =
    ( model, Cmd.none )



-- EXPORT


toSession : Model -> Session
toSession model =
    model.session
